package net.exszr.feather_fly;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.java.JavaPlugin;

public final class FeatherFly extends JavaPlugin implements Listener {
	List<String> playerlist;
	
	@Override
	public void onEnable(){
		//Configが"なかったら"コピーする
		getConfig().options().copyDefaults(true);
		saveConfig();
		
		getLogger().info("FeatherFly is Enabled!");
		playerlist=new ArrayList<String>();
		
		//Eventは登録しないと動かないよ!!!
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	@Override
	public void onDisable() {
		getLogger().info("FeatherFly is Disabled!");
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		//コマンドはコンソールからも送れるから、Playerとは限らない
		if (!(sender instanceof Player)){
			sender.sendMessage("Player専用です");
			return true;
		}
		
		
		Player player = (Player)sender;
		if (cmd.getName().equalsIgnoreCase("ff")){
			if (args.length < 1){
				sender.sendMessage("Please parameter.");
				sender.sendMessage("/ff help");
				return true;
			}else if (args[0].equalsIgnoreCase("use")){
				PlayerInventory inv = player.getInventory();
				ItemStack featherstack = new ItemStack(Material.FEATHER , getConfig().getInt("busycount"));
				//Inventory内に対象アイテムが必要数以上ある場合
				if (getAllItemsAmount(inv, featherstack) >= featherstack.getAmount()){
					inv.removeItem(featherstack);
					player.setAllowFlight(true);
					player.sendMessage(getConfig().getString("emessage").replaceAll("&([a-f0-9])", "\u00A7$1"));
					getServer().broadcastMessage(getConfig().getString("bmessage").replaceAll("&p", player.getName()).replaceAll("&([a-f0-9])", "\u00A7$1"));
					playerlist.add(player.getName());
					
					return true;
				}else{
					player.sendMessage(ChatColor.RED + getConfig().getString("nothave1").replaceAll("&([a-f0-9])", "\u00A7$1")  + getConfig().getInt("busycount"));
					player.sendMessage(ChatColor.RED + getConfig().getString("nothave2").replaceAll("&([a-f0-9])", "\u00A7$1"));
					return true;
				}
			}else if (args[0].equalsIgnoreCase("end")){
				player.setAllowFlight(false);
				player.setFlying(false);
				player.sendMessage(getConfig().getString("dmessage").replaceAll("&([a-f0-9])", "\u00A7$1"));
				
				for (int i = 0; i < playerlist.size(); i++){
					//player.getName()はStringだから==では比較不可
					if (playerlist.get(i).equalsIgnoreCase(player.getName())){
						playerlist.remove(i);
						break;
					}
				}
				return true;
			}else if (args[0].equalsIgnoreCase("list")){
				sender.sendMessage("Player List");
				for (String p : playerlist)
					sender.sendMessage(p);
				return true;
				
			}else if (args[0].equalsIgnoreCase("help")){
				sender.sendMessage(ChatColor.RED + "Feather Fly help");
				sender.sendMessage("/ff use:Enable for flying.");
				sender.sendMessage("/ff end:Disable for flying.");
				
				return true;
			}
		}
		return false;
	}
	
	/**
	 * パラメータのInventory内にItemStackの対象アイテムが合計何個あるか返す
	 * @param inv
	 * @param item
	 * @return ItemStackの対象Materialの合計数
	 */
	public int getAllItemsAmount(Inventory inv, ItemStack item){
		ItemStack[] inv_items = inv.getContents();
		
		int count = 0;
		for(int i = 0;i < inv_items.length;i++){
			//nullの時は次へ
			if (inv_items[i] == null){
				continue;
			}
			
			
			if (inv_items[i].getType() == item.getType()){
				count = count + inv_items[i].getAmount();
			}
		}
		
		return count;
	}
	
	@EventHandler
	public void PlayerQuit(PlayerQuitEvent event)
	{
		Player player = event.getPlayer();
		player.setAllowFlight(false);
		player.setFlying(false);
		for (int i = 0; i < playerlist.size(); i++){
			//player.getName()はStringだから==では比較不可
			if (playerlist.get(i).equalsIgnoreCase(player.getName())){
				playerlist.remove(i);
				break;
			}
		}
	}
}